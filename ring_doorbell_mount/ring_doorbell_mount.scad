$fn=64;

$plateWidth        =  51.75;  // Ring doorbell housing width
// changed from 49.00 to 51.75, increase of 2.75mm
$plateLength       = 116.50;  // Ring doorbell housing length
$plateThickness    =   7.00;  // Doorbell plate thickness
$plateRadius       =   2.00;  // Doorbell plate corner radius
$mountHoleDistance = 105.00;  // Distance between doorbell mounting holes
$mountHoleDiameter =   4.00;  // Doorbell mounting hole diameter
$wireCutoutWidth   =  30.00;  // Wire cutout width
$wireCutoutLength  =  50.00;  // Wire cutout length
$wireCutoutOffset  =  30.00;  // Wire cutout offset, from center of bottom mounting hole
// plate width changed, added 2.75mm
// - need to add 1.25 to 1st
// - need to add 2.75 to 2nd and 3rd
$moldWidth_1       =  13.75;  // 1st inward cut from left, was 12.50
$moldWidth_2       =  19.50;  // 2nd inward cut from left, was 16.75
$moldWidth_3       =  24.00;  // 3rd inward cut from left, was 21.25
$moldThick_1       =   4.60;  // 1st cut remaining thickness
$moldThick_2       =   2.60;  // 2nd cut remaining thickness
$moldThick_3       =   1.20;  // 3rd cut remaining thickness
  
module workingBody(){
  difference(){
    cube([$plateWidth, $plateThickness, $plateLength]);
    translate([ 0.00, 0.00, 0.00])
      cube([$plateRadius, $plateThickness, $plateRadius]);
    translate([$plateWidth - $plateRadius, 0.00, 0.00])
      cube([$plateRadius, $plateThickness, $plateRadius]);
    translate([ 0.00, 0.00, $plateLength - $plateRadius])
      cube([$plateRadius, $plateThickness, $plateRadius]);
    translate([$plateWidth - $plateRadius, 0.00, $plateLength - $plateRadius])
      cube([$plateRadius, $plateThickness, $plateRadius]);
    }
    translate([$plateRadius, $plateThickness, $plateRadius])
      rotate([90,0,0])
        cylinder(h=$plateThickness, r=$plateRadius);
    translate([$plateWidth - $plateRadius, $plateThickness, $plateRadius])
      rotate([90,0,0])
        cylinder(h=$plateThickness, r=$plateRadius);
    translate([$plateRadius, $plateThickness, $plateLength - $plateRadius])
      rotate([90,0,0])
        cylinder(h=$plateThickness, r=$plateRadius);
    translate([$plateWidth - $plateRadius, $plateThickness, $plateLength - $plateRadius])
      rotate([90,0,0])
        cylinder(h=$plateThickness, r=$plateRadius);
}

module wireCutout(){
  difference(){
    cube([$wireCutoutWidth,$plateThickness,$wireCutoutLength]);
    translate([ 0.00, 0.00, 0.00])
      cube([$plateRadius, $plateThickness, $plateRadius]);
    translate([$wireCutoutWidth - $plateRadius, 0.00, 0.00])
      cube([$plateRadius, $plateThickness, $plateRadius]);
    translate([0.00, 0.00, $wireCutoutLength - $plateRadius])
      cube([$plateRadius, $plateThickness, $plateRadius]);
    translate([$wireCutoutWidth - $plateRadius, 0.00, $wireCutoutLength - $plateRadius])
      cube([$plateRadius, $plateThickness, $plateRadius]);
  }
    translate([$plateRadius,$plateThickness, $plateRadius])
      rotate([90,0,0])
        cylinder(h=$plateThickness, r=$plateRadius);
    translate([$wireCutoutWidth - $plateRadius, $plateThickness, $plateRadius])
      rotate([90,0,0])
        cylinder(h=$plateThickness, r=$plateRadius);
    translate([$plateRadius, $plateThickness, $wireCutoutLength - $plateRadius])
      rotate([90,0,0])
        cylinder(h=$plateThickness, r=$plateRadius);
    translate([$wireCutoutWidth - $plateRadius, $plateThickness, $wireCutoutLength - $plateRadius])
      rotate([90,0,0])
        cylinder(h=$plateThickness, r=$plateRadius);
}

module mountHole() {
  translate([  0.00, $plateThickness, 0.00])
    rotate([90,0,0])
      cylinder(h=$plateThickness, d=$mountHoleDiameter);
  translate([  0.00, $plateThickness, $mountHoleDistance])
    rotate([90,0,0])
      cylinder(h=$plateThickness, d=$mountHoleDiameter);
}

module mainBody() {
  workingBody();
  // slant length = 3.00mm
}
    
difference(){
  mainBody();
  // Wire cutout
  translate([($plateWidth-$wireCutoutWidth)/2, 0.0, ($plateLength - $mountHoleDistance)/2 + $wireCutoutOffset])
    wireCutout();
  translate([$plateWidth/2, 0.00, ($plateLength - $mountHoleDistance)/2])
    mountHole();
  translate([0.00, $plateThickness-$moldThick_1, 0.00]) cube([$moldWidth_1 - 2.00, $moldThick_1, $plateLength]);
  translate([0.00, $plateThickness-$moldThick_2, 0.00]) cube([$moldWidth_2 - 2.00, $moldThick_2, $plateLength]);
  translate([0.00, $plateThickness-$moldThick_3, 0.00]) cube([$moldWidth_3 - 2.00, $moldThick_3, $plateLength]);
  translate([$moldWidth_1 - 2.00, $plateThickness - $moldThick_1, 0.00]) rotate([0,0,45]) cube([ 3.00, 3.00, $plateLength]);
  translate([$moldWidth_2 - 2.00, $plateThickness - $moldThick_2, 0.00]) rotate([0,0,45]) cube([ 3.00, 3.00, $plateLength]);
  translate([$moldWidth_3 - 2.00, $plateThickness - $moldThick_3, 0.00]) rotate([0,0,45]) cube([ 3.00, 3.00, $plateLength]);
}

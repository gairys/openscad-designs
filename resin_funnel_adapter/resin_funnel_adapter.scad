$fn=64;

translate([0,0,0])
  difference(){
    cylinder(h=16,d=22.50);
    cylinder(h=16,d=17.50);
  }

translate([0,0,25])
  difference(){
    cylinder(h=15,d=33.00);
    cylinder(h=15,d=28.00);
  }


translate([0,0,15])
  difference(){
    cylinder(h=11,d=33.00);
    cylinder(h=11,d1=17.50,d2=28.00);
  }

$fn=4;

difference() {
  cylinder(h=60,r1=50,r2=30);
  translate([0,0,57.50+12.5])
    rotate([0,0,45])
      cube([30,30,25],center=true);
}
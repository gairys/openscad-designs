$fn = 32;

// All measurements in mm
gap = 10.00;
dia =  6.50;
head = 12.25;
pipe = 22.50;

module wing(h) {
  cube([8,10,h]);
  translate([4,0,0])
    cylinder(h = h, d = 8);
  translate([4,10,0])
    cylinder(h = h, d = 8);
}

module center(h) {
  difference() {
    cylinder(h = h, d = 20);
    cylinder(h = h, d = dia);
    translate([0,0,5])
      cylinder(h = h, d = head, $fn = 6);
  }
}

module body() {
  center(10);
  translate([-4,9.5,0])
    wing(12);
  translate([-4,-19.5,0])
    wing(12);
}

body();

module test_body() {
  difference() {
    body();
    translate([-30,-30,0])
      cube([60,60,4]);
    translate([-30,-30,7])
      cube([60,60,20]);
  }
}

//test_body();

// All measurements in mm
gap = 10.00;
dia =  6.50;
head = 10.50;
pipe = 22.50;

difference() {
  cylinder(h = 8.0, d = 20.00);
  cylinder(h = 8.0, d = dia);
  translate([0,10,-8])
    rotate([90,0,0])
      cylinder(h = 20, d = pipe);
}

$fn = 64;

$square=40.0;   // base width and length
$height=65.0;   // overall height
$screw=3.0;     // screw hole diameter

module foot() {
  translate([$square/2,$square/2,0])
    cylinder(h=$height,d=26.0);
}

module base() {
  difference() {
    cube([$square,$square,2.5]);
    translate([0,0,0])
      cube([4.0,4.0,2.5]);
    translate([$square-4.0,0,0])
      cube([4.0,4.0,2.5]);
    translate([0,$square-4.0,0])
      cube([4.0,4.0,2.5]);
    translate([$square-4.0,$square-4.0,0])
      cube([4.0,4.0,2.5]);
    translate([8,8,0])
      cylinder(h = 3.0, d = $screw);
    translate([$square-8,8,0])
      cylinder(h = 3.0, d = $screw);
    translate([8,$square-8,0])
      cylinder(h = 3.0, d = $screw);
    translate([$square-8,$square-8,0])
      cylinder(h = 3.0, d = $screw);
  }
  translate([4.0,4.0,0])
    cylinder(h = 2.5, d = 8.0);
  translate([$square-4,4,0])
    cylinder(h = 2.5, d = 8.0);
  translate([4,$square-4,0])
    cylinder(h = 2.5, d = 8.0);
  translate([$square-4,$square-4,0])
    cylinder(h = 2.5, d = 8.0);
}

base();
foot();
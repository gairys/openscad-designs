$fn=64;

module corner_leg() {
  difference() {
    cube([40,40,80]);
    translate([0,0,0])
      cube([8,8,80]);
    translate([32,0,0])
      cube([8,8,80]);
    translate([0,32,0])
      cube([8,8,80]);
    translate([32,32,0])
      cube([8,8,80]);
  }
  translate([8,8,0])
    cylinder(h=80,d=16);
  translate([32,8,0])
    cylinder(h=80,d=16);
  translate([8,32,0])
    cylinder(h=80,d=16);
  translate([32,32,0])
    cylinder(h=80,d=16);
}

difference() {
  corner_leg();
  translate([8,8,63.75])
    cube([40,40,16.25]);
}
$fn = 64;

max_h = 63.75;

translate([0, 0, 0])
  cylinder(h = max_h, d = 15);
translate([0, 50, 0])
  cylinder(h = max_h, d = 15);
translate([-7.5, 0, 0])
  cube([15, 50, max_h]);

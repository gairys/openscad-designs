$fn=32;

difference() {
  cube([25,25,25]);
  cube([25, 7, 7]);
  translate([0,0,7])
    rotate([15,0,0])
      cube([25,25,25]);
  translate([0,0,11])
    cube([25,25,25]);
}

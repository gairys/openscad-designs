$fn=64;

outer_diameter = 13.75;
inner_hole = 4.75;
height = 2.75;

difference(){
    cylinder(h=height, d=outer_diameter);
    cylinder(h=height, d=inner_hole);
}

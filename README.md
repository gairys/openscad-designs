# OpenSCAD Designs

I have a few things I've designed through the years using OpenSCAD. This is my attempt to put all of these projects in a central location. Some of these projects are designed with formulas and best practices in mind, and some were just thrown together as fast as possible.

If you choose to use any of these designs, you are using them at your own risk.

## Projects and Descriptions

### Bathroom Chair Parts

While my wife was recovering from a leg injury, she needed to use a chair in the shower. During it's use, one of the plastic wingnuts that came with the chair fell apart. I don't have the make or model of the chair handy as we purchased it a few years ago from Amazon.

- `wingnut.scad` - Designed to replace the wingnut by inserting a standard nut that can be purchased from Home Depot or Lowe's.
- `washer.scad`  - Designed to match the contour of the pipe and to be used in conjunction with `wingnut.scad`.

### Custom Feet

This is a collection of custom feet I designed for different purposes. When I first made my monitor shelf for my desk, I planned on using these feet, until I realized I'd have to screw all of them to the shelf and if I didn't like the placement of the feet, or needed to relocate supports after changing monitors, I'd have to drill a bunch of new holes. But it seemed like a decent enough design to keep around in case I needed it again.

### Custom Washer

I think everyone at one time or another has needed a custom plastic washer. I've lost count of how many times I've used this file as it was one of my first basic designs to learn OpenSCAD.

### Dishwasher Door Spacer

For some reason, my dishwasher "walks" inside the cutout and eventually makes contact with the left side. When this happens, the dishwasher door no longer opens. This was designed to go into that gap in two places and "straddle" the outside trim to create a makeshift "ramp" for the door to use instead of making contact with the trim and causing damage.

### Drawer Divider

Before playing around with Gridfinity, I created this simple divider that can be set to any length and held in place using double-sided tape.

### Monitor Shelf

I wanted to lift my monitors off my desk with enough room to put my keyboard and mouse under when not in use. Instead of spending a ton of money I went to Home Depot and purchased a cheap 5 foot wooden shelf. These files create the "feet" to support everything.

- `center_leg.scad` - Designed to support the back outer ledge of the shelf, away from where I sit.
- `corner_leg.scad` - Designed to support each corner of the shelf.
- `inner_leg.scad`  - Designed to be placed inside the shelf under the monitor legs to add additional support and prevent sagging.

### Patio Umbrella Bushing

The pole for the replacement patio umbrella was a little smaller in diameter than the original so I created a bushing for the top and bottom of the holder to better clamp the umbrella pole.

### Portable A/C Riser

In my old house, my office was in the basement, which would get very hot during the summer. We purchased a portable air conditioner, but the condensation drain was too close to the floor to place anything underneath to drain the moisture on a weekly basis. This was a quick design to place under the A/C unit, one for each caster, and would raise it enough for me to get a disposable turkey pan under it to drain it.

### Resin funnel adapter

I really like [this design](https://www.thingiverse.com/thing:3135135) for straining my printer resin back into the bottle. The issue I found was that the bottom of the funnel didn't fit my bottle of Anycubic resin. That's where this adapter comes in.

### Ring Doorbell Mount

My MiL had a very small doorbell and we replaced it with a wider Ring Doorbell. The problem was the trim around the door wasn't flat. So I designed a mount for the doorbell that would fit the contour of the door trim and provide enough space for the wires to be hidden inside the mount without pinching.

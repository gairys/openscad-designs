$fn=64;

module outer() {
  difference () {
    cylinder(h = 30.00, d = 61.75);
    cylinder(h = 30.00, d = 47.75);
  }
}

difference() {
  cylinder(h = 31.00, d = 61.75);
  cylinder(h = 31.00, d = 38.50);
  translate([0,0,5])
    outer();
}